class Person{
    constructor(name,age,salary,sex){
        this.name=name;
        this.age=age;
        this.salary=salary;
        this.sex=sex;
    }

    static quickSort(personArray,field,orderOfSorting){
        let tempArray=[].concat(personArray);
        if(tempArray.length<=1){
            return tempArray;
        }
        let pivot=tempArray[0][field];
        let pivotItems=tempArray[0];

        let left=[];
        let right=[];

        for(let i=1;i<tempArray.length;i++){
            if(orderOfSorting=='asc'){
                tempArray[i][field]<pivot?left.push(tempArray[i]):right.push(tempArray[i]);
            }else{
                tempArray[i][field]<pivot?right.push(tempArray[i]):left.push(tempArray[i]);
            }
        }
        return Person.quickSort(left,field,orderOfSorting).concat(pivotItems,Person.quickSort(right,field,orderOfSorting));

    }

}
let person1=new Person('Pranshu',11,5000,'M');
let person2=new Person('Ram',22,6000,'M');
let person3=new Person('himanshu',10,5000,'M');
let person4=new Person('shyam',25,6000,'M');
let person5=new Person('nitya',27,6500,'N');

const personArray=[person1,person2,person3,person4,person5];
console.log(Person.quickSort(personArray,'salary','asc'));


